#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "colour-convert.h"
#include <helper_timer.h>

#define GRID_SIZE 600

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__device__ unsigned char clip_rgb_gpu(int x);
__global__ void rgb2yuvKernel(unsigned char *d_r, unsigned char *d_g, unsigned char *d_b, 
                              unsigned char *d_y, unsigned char *d_u, unsigned char *d_v, 
                              int pixel_count) 
{
    
    //int index = threadIdx.x + blockIdx.x * blockDim.x;
    
    int blockId   = blockIdx.y * gridDim.x + blockIdx.x;
    int index = blockId * blockDim.x + threadIdx.x; 
    if(index < pixel_count)
    {
      d_y[index] = clip_rgb_gpu(round( 0.299*d_r[index] + 0.587*d_g[index] + 0.114*d_b[index]));
      d_u[index] = clip_rgb_gpu(round(-0.169*d_r[index] - 0.331*d_g[index] + 0.499*d_b[index] + 128));
      d_v[index] = clip_rgb_gpu(round( 0.499*d_r[index] - 0.418*d_g[index] - 0.0813*d_b[index] + 128));
    }
	
}



__global__ void yuv2rgbKernel(unsigned char * d_y, unsigned char *d_u, unsigned char *d_v, 
                              int *y, int *d_cb,int *d_cr, 
                              unsigned char *d_r, unsigned char *d_g, unsigned char *d_b, 
                              int pixel_count) 
{
    int blockId   = blockIdx.y * gridDim.x + blockIdx.x;
    int i = blockId * blockDim.x + threadIdx.x; 
    if(i < pixel_count)
    {
        d_cb[i]=d_u[i]-128;  //d_cb[i]
        d_cr[i]=d_v[i]-128; //d_cr[i]
        y[i]=d_y[i];
                
        d_r[i]  =  clip_rgb_gpu(round(y[i] + 1.402*d_cr[i]));
        d_g[i]  =  clip_rgb_gpu(round(y[i] - 0.344*d_cb[i] - 0.714*d_cr[i])); 
        d_b[i]  =  clip_rgb_gpu(round(y[i] + 1.772*d_cb[i]));

   }
}

__device__ unsigned char clip_rgb_gpu(int x)
{
    if(x > 255)
        return 255;
    if(x < 0)
        return 0;

    return (unsigned char)x;
}


YUV_IMG rgb2yuvGPU(PPM_IMG img_in, int GridSize, int Threads, int is_tabular)
{

    YUV_IMG img_out;
    unsigned char *d_r, *d_g, *d_b;
    unsigned char *d_y, *d_u, *d_v;
    StopWatchInterface *timer=NULL;
  
    int size = (img_in.w * img_in.h) * sizeof(char);

    int GridY = size/GridSize/Threads;
    const dim3 BlockDim(GridSize+10, GridY+10);

    img_out.w=img_in.w;
    img_out.h=img_in.h;    
    
    
    //Time to measure space allocation time  
    sdkCreateTimer(&timer);
    sdkStartTimer(&timer);

    // Alloc space for device copies of r, g, b
    gpuErrchk(cudaMalloc((void **)&d_r, size));
    gpuErrchk(cudaMalloc((void **)&d_g, size));
    gpuErrchk(cudaMalloc((void **)&d_b, size));

    // Alloc space for device copies of y, u, v
    gpuErrchk(cudaMalloc((void **)&d_y, size));
    gpuErrchk(cudaMalloc((void **)&d_u, size));
    gpuErrchk(cudaMalloc((void **)&d_v, size));

    sdkStopTimer(&timer);

    if(!is_tabular)
      printf("Space Allocation GPU: %f (ms)\n", sdkGetTimerValue(&timer));
    else
      printf("%f\t|", sdkGetTimerValue(&timer));

    sdkDeleteTimer(&timer);


    //Data copy to GPU  
    sdkCreateTimer(&timer);
    sdkStartTimer(&timer);
    
    // Copy each pixel component to gpu memory    
    cudaMemcpy(d_r, img_in.img_r, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_g, img_in.img_g, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, img_in.img_b, size, cudaMemcpyHostToDevice);
    
    sdkStopTimer(&timer);
    
    if(!is_tabular)
      printf("Data copy Main->GPU: %f (ms)\n", sdkGetTimerValue(&timer));
    else
      printf("%f\t|", sdkGetTimerValue(&timer));
    sdkDeleteTimer(&timer);

    //printf("Image dimension:%d\n",img_in.w*img_in.h);
    //printf("%d\t\t|",img_in.w*img_in.h);
    rgb2yuvKernel<<<BlockDim, Threads>>>(d_r, d_g, d_b, d_y, d_u, d_v, size);//Launch the Kernel
    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
    
    // Allocate memory for image out
    img_out.img_y = (unsigned char *)malloc(size*sizeof(char));
    img_out.img_u = (unsigned char *)malloc(size*sizeof(char));
    img_out.img_v = (unsigned char *)malloc(size*sizeof(char));

    //Timer for GPU to mainmemory
    sdkCreateTimer(&timer);
    sdkStartTimer(&timer);

    // Copy each pixel component to main memory from gpu memory
    gpuErrchk(cudaMemcpy(img_out.img_y, d_y, size, cudaMemcpyDeviceToHost));
    gpuErrchk(cudaMemcpy(img_out.img_u, d_u, size, cudaMemcpyDeviceToHost));
    gpuErrchk(cudaMemcpy(img_out.img_v, d_v, size, cudaMemcpyDeviceToHost));
    
    if(!is_tabular)
      printf("Data copy GPU->Main:: %f (ms)\n", sdkGetTimerValue(&timer));
    else
      printf("%f\t|", sdkGetTimerValue(&timer));
    sdkDeleteTimer(&timer);
   
    // Free cuda memory
    cudaFree(d_y); cudaFree(d_u); cudaFree(d_v);
    cudaFree(d_r); cudaFree(d_g); cudaFree(d_b);

    return img_out;
}




PPM_IMG yuv2rgbGPU(YUV_IMG img_in, int GridSize, int Threads, int is_tabular)
{
    PPM_IMG img_out;

    unsigned char *d_r, *d_g, *d_b;
    unsigned char *d_y, *d_u, *d_v;
    int *y, *d_cb, *d_cr;   

    int size = (img_in.w * img_in.h) * sizeof(char);
    int GridY = size/GridSize/Threads;
    const dim3 BlockDim(GridSize+10, GridY+10);


    img_out.w=img_in.w;
    img_out.h=img_in.h;

    // Alloc space for device copies of y, u, v
    cudaMalloc((void **)&d_y, size);
    cudaMalloc((void **)&d_u, size);
    cudaMalloc((void **)&d_v, size);
    
    
    cudaMalloc((void **)&y, size * sizeof(int));
    cudaMalloc((void **)&d_cb, size * sizeof(int));
    cudaMalloc((void **)&d_cr, size * sizeof(int));

    // Alloc space for device copies of r, g, b
    cudaMalloc((void **)&d_r, size);
    cudaMalloc((void **)&d_g, size);
    cudaMalloc((void **)&d_b, size);

    // Copy each pixel component to gpu memory    
    cudaMemcpy(d_y, img_in.img_y, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_u, img_in.img_u, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_v, img_in.img_v, size, cudaMemcpyHostToDevice);
    
    
    //printf("Image dimension:%d\n",img_in.w*img_in.h);
    yuv2rgbKernel<<<BlockDim, Threads>>>(d_y, d_u, d_v, 
                                y,  d_cb, d_cr,
                                d_r, d_g, d_b, size);//Launch the Kernel
    
    // Allocate memory for image out
    img_out.img_r = (unsigned char *)malloc(size);
    img_out.img_g = (unsigned char *)malloc(size);
    img_out.img_b = (unsigned char *)malloc(size);

    // Copy each pixel component to main memory from gpu memory
    cudaMemcpy(img_out.img_r, d_r, size, cudaMemcpyDeviceToHost);
    cudaMemcpy(img_out.img_g, d_g, size, cudaMemcpyDeviceToHost);
    cudaMemcpy(img_out.img_b, d_b, size, cudaMemcpyDeviceToHost);
   
    // Free cuda memory
    cudaFree(d_y); cudaFree(d_u); cudaFree(d_v);
    cudaFree(y);   cudaFree(d_cb); cudaFree(d_cr);
    cudaFree(d_r); cudaFree(d_g); cudaFree(d_b);

    return img_out;
}

//Convert RGB to YUV444, all components in [0, 255]
YUV_IMG rgb2yuv(PPM_IMG img_in)
{
    YUV_IMG img_out;
    int i;//, j;
    unsigned char r, g, b;
    unsigned char y, cb, cr;
    
    img_out.w = img_in.w;
    img_out.h = img_in.h;

    img_out.img_y = (unsigned char *)malloc(sizeof(unsigned char)*img_out.w*img_out.h);
    img_out.img_u = (unsigned char *)malloc(sizeof(unsigned char)*img_out.w*img_out.h);
    img_out.img_v = (unsigned char *)malloc(sizeof(unsigned char)*img_out.w*img_out.h);
 
    for(i = 0; i < img_out.w*img_out.h; i ++){
        r = img_in.img_r[i];
        g = img_in.img_g[i];
        b = img_in.img_b[i];
        
        y  = (unsigned char)( 0.299*r + 0.587*g +  0.114*b);
        cb = (unsigned char)(-0.169*r - 0.331*g +  0.499*b + 128);
        cr = (unsigned char)( 0.499*r - 0.418*g - 0.0813*b + 128);


        
        img_out.img_y[i] = y;
        img_out.img_u[i] = cb;
        img_out.img_v[i] = cr;
    }
    
    return img_out;
}

unsigned char clip_rgb(int x)
{
    if(x > 255)
        return 255;
    if(x < 0)
        return 0;

    return (unsigned char)x;
}

//Convert YUV to RGB, all components in [0, 255]
PPM_IMG yuv2rgb(YUV_IMG img_in)
{
    PPM_IMG img_out;
    int i;
    int  rt,gt,bt;
    int y, cb, cr;
    
    
    img_out.w = img_in.w;
    img_out.h = img_in.h;
    img_out.img_r = (unsigned char *)malloc(sizeof(unsigned char)*img_out.w*img_out.h);
    img_out.img_g = (unsigned char *)malloc(sizeof(unsigned char)*img_out.w*img_out.h);
    img_out.img_b = (unsigned char *)malloc(sizeof(unsigned char)*img_out.w*img_out.h);

    for(i = 0; i < img_out.w*img_out.h; i ++){
        y  = (int)img_in.img_y[i];
        cb = (int)img_in.img_u[i] - 128;
        cr = (int)img_in.img_v[i] - 128;
        
        rt  = (int)( y + 1.402*cr);
        gt  = (int)( y - 0.344*cb - 0.714*cr); 
        bt  = (int)( y + 1.772*cb);

        img_out.img_r[i] = clip_rgb(rt);
        img_out.img_g[i] = clip_rgb(gt);
        img_out.img_b[i] = clip_rgb(bt);
    }
    
    return img_out;
}
