#include <stdio.h>
#include <string.h>
#include <stdlib.h>
// CUDA Runtime
#include <cuda_runtime.h>
// Utility and system includes
#include <helper_cuda.h>
// helper for shared that are common to CUDA Samples
#include <helper_functions.h>
#include <helper_timer.h>

#include "colour-convert.h"
#define MAX_CONVERSION_LOSS 2

int IS_TABULAR = 0;

void run_cpu_color_test(PPM_IMG img_in);
void gpu_tests(PPM_IMG img_in);
void run_gpu_color_test(PPM_IMG img_in, bool save, int GridSize, int Threads, int is_tabular);
bool confirm_gpu_rgb2yuv(YUV_IMG gpu_img_in, PPM_IMG img_in);
bool confirm_gpu_yuv2rgb(PPM_IMG gpu_img_in,PPM_IMG img_in);
PPM_IMG generate_test();
void debug_print_arrays(unsigned char *array1, unsigned char *array2, int size);

//---------------------------------------------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    PPM_IMG img_ibuf_c;
    if(argc > 1){
      IS_TABULAR = atoi(argv[1]);
    }

    printf("Running colour space converter .\n");
    img_ibuf_c = read_ppm("in.ppm");
    printf("----------------------------CPU-------------------------------\n");
    run_cpu_color_test(img_ibuf_c);
    printf("----------------------------GPU-------------------------------\n");
    gpu_tests(img_ibuf_c);
    free_ppm(img_ibuf_c);
    
    return 0;
}
//---------------------------------------------------------------------------------------------------------------------
void gpu_tests(PPM_IMG img_in)
{
    int x, y;
    printf("----Warm up Test----\n");
    rgb2yuvGPU(img_in, 1000, 1024, 0); //Start RGB 2 YUV

    printf("----End of warmup test----\n\n");
  
    printf("--------- GPU Processing times based on Blocks and Threads -----------\n");
    if(IS_TABULAR){
      printf("\n\nBlocks\t|Threads|Space Alloc\t|DataToGPU\t|DataToMain\t|R-Y Tot Time\t|Y-R Tot time\t|R-Y min Loss\t|Y-R min Loss\t|\n");
      printf("------------------------------------------------------------------------------------------------------------------------------------\n");
    }
    for(y=1000;y<5000;y+=1000){
      for(x=2;x<=1024;x*=2)  
      {  
          if(!IS_TABULAR){
           printf("--------------------------------------------------------------\n"); 
           printf("Running gpu test NumBlocks:%d, NumThreads:%d\n",y,x);
          }
          else
            printf("%d\t|%d\t|",y,x);
          
          run_gpu_color_test(img_in, false, y, x, IS_TABULAR);
          printf("\n");    
      }  
    }  
    //run_gpu_color_test(img_in, true, 1000,1024, 0);    

}
//---------------------------------------------------------------------------------------------------------------------
void run_gpu_color_test(PPM_IMG img_in, bool save, int GridSize, int Threads, int is_tabular)
{
    StopWatchInterface *timer=NULL;
    PPM_IMG img_obuf_rgb;
    YUV_IMG img_obuf_yuv;
 
    if(!is_tabular){
      printf("Starting GPU processing...\n");
      printf("Threads:%d\n", Threads); 
    }

    //-------------------------------------------------------------
    sdkCreateTimer(&timer);
    sdkStartTimer(&timer);

    img_obuf_yuv = rgb2yuvGPU(img_in, GridSize, Threads, is_tabular); //Start RGB 2 YUV
    sdkStopTimer(&timer);
    
    if(!is_tabular)
      printf("RGB to YUV conversion time(GPU): %f (ms)\n", sdkGetTimerValue(&timer));
    else
      printf("%f\t|", sdkGetTimerValue(&timer));
    sdkDeleteTimer(&timer);
    //-------------------------------------------------------------
    sdkCreateTimer(&timer);
    sdkStartTimer(&timer);
	
    img_obuf_rgb = yuv2rgbGPU(img_obuf_yuv, GridSize, Threads, is_tabular); //Start YUV 2 RGB

    sdkStopTimer(&timer);

    if(!is_tabular)
      printf("YUV to RGB conversion time(GPU): %f (ms)\n", sdkGetTimerValue(&timer));
    else
      printf("%f\t|", sdkGetTimerValue(&timer));
    sdkDeleteTimer(&timer);    
    //-------------------------------------------------------------
	
	confirm_gpu_rgb2yuv(img_obuf_yuv, img_in);
   	confirm_gpu_yuv2rgb(img_obuf_rgb, img_in);

    if(save)    
    {
        write_yuv(img_obuf_yuv, "out_gpu_yuv.yuv");
        write_ppm(img_obuf_rgb, "out_gpu_rgb.ppm");
    }
    
    free_ppm(img_obuf_rgb); //Uncomment these when the images exist
    free_yuv(img_obuf_yuv);

}
//---------------------------------------------------------------------------------------------------------------------
void run_cpu_color_test(PPM_IMG img_in)
{
    StopWatchInterface *timer=NULL;
    PPM_IMG img_obuf_rgb;
    YUV_IMG img_obuf_yuv;
    
    
    printf("Starting CPU processing...\n");
  
    sdkCreateTimer(&timer);
    sdkStartTimer(&timer);

    img_obuf_yuv = rgb2yuv(img_in); //Start RGB 2 YUV

    sdkStopTimer(&timer);

    printf("RGB to YUV conversion time: %f (ms)\n", sdkGetTimerValue(&timer));
    sdkDeleteTimer(&timer);

   

    sdkCreateTimer(&timer);
    sdkStartTimer(&timer);

    img_obuf_rgb = yuv2rgb(img_obuf_yuv); //Start YUV 2 RGB

    sdkStopTimer(&timer);


    printf("YUV to RGB conversion time: %f (ms)\n", sdkGetTimerValue(&timer));
    sdkDeleteTimer(&timer);    

	
	confirm_gpu_rgb2yuv(img_obuf_yuv, img_in);
	confirm_gpu_yuv2rgb(img_obuf_rgb, img_in);

    write_yuv(img_obuf_yuv, "out_cpu_yuv.yuv");
    write_ppm(img_obuf_rgb, "out_cpu_rgb.ppm");
    
    free_ppm(img_obuf_rgb);
    free_yuv(img_obuf_yuv);
    
}
//---------------------------------------------------------------------------------------------------------------------
void debug_print_arrays(unsigned char *array1, unsigned char *array2, int size)
{
    int x;
    int DebugSize=4;
    if(size>10) 
        size=10;

    printf("\nDebug\n");
    for(x=0;x<DebugSize;x++)   
        printf("%d|",array1[x]);
    printf("\n");
    for(x=0;x<DebugSize;x++)   
        printf("%d|",array2[x]);
    printf("\n");

}
//---------------------------------------------------------------------------------------------------------------------
bool confirm_gpu_rgb2yuv(YUV_IMG gpu_img_in, PPM_IMG img_in) //Place code here that verifies your conversion
{
    YUV_IMG img_obuf_yuv;
    int size = (img_in.w * img_in.h) * sizeof(char);
    img_obuf_yuv = rgb2yuv(img_in); //Start RGB 2 YUV
    int x;


    if(gpu_img_in.w==img_obuf_yuv.w)
        if(gpu_img_in.h==img_obuf_yuv.h)      
        {           
            for(x=0;x<size;x++)               
            {
                if(abs(gpu_img_in.img_y[x]-img_obuf_yuv.img_y[x])>MAX_CONVERSION_LOSS ||        
                   abs(gpu_img_in.img_u[x]-img_obuf_yuv.img_u[x])>MAX_CONVERSION_LOSS || 
                   abs(gpu_img_in.img_v[x]-img_obuf_yuv.img_v[x])>MAX_CONVERSION_LOSS)        
                   {
                        printf("Error rgb2yuv wrong pixel pos:%d abs(y):%d abs(u):%d abs(v):%d\n",x,
                        abs(gpu_img_in.img_y[x]-img_obuf_yuv.img_y[x]),
                        abs(gpu_img_in.img_u[x]-img_obuf_yuv.img_u[x]),
                        abs(gpu_img_in.img_v[x]-img_obuf_yuv.img_v[x]));
                        printf("Error rgb2yuv Original image values:y:%d u:%d v:%d\n",
                                img_obuf_yuv.img_y[x],img_obuf_yuv.img_u[x],img_obuf_yuv.img_v[x]);
                        printf("Error rgb2yuv Converted image values:y:%d u:%d v:%d\n",
                                gpu_img_in.img_y[x],gpu_img_in.img_u[x],gpu_img_in.img_v[x]);
        
                        return false;
                   }
            }       
            if(!IS_TABULAR)
              printf("Successfully confirmed confirm_gpu_rgb2yuvGPU!\n"); 
            else
              printf("Success\t|");           
            return true;
        }   
               
    return false;

}
//---------------------------------------------------------------------------------------------------------------------
bool confirm_gpu_yuv2rgb(PPM_IMG gpu_img_in,PPM_IMG img_in) //Place code here that verifies your conversion
{           
    int x=0;
    int size = (img_in.w * img_in.h) * sizeof(char);

     
    
    if(gpu_img_in.w==img_in.w)
        if(gpu_img_in.h==img_in.h)
        {
                        
            for(x=0;x<size;x++)  
            {     
                //printf("abs:%d,%d,%d\n",abs(gpu_img_in.img_r[x]-img_in.img_r[x]),
                //abs(gpu_img_in.img_g[x]-img_in.img_g[x]),abs(gpu_img_in.img_b[x]-img_in.img_b[x])); 
          
                if(abs(gpu_img_in.img_r[x]-img_in.img_r[x])>MAX_CONVERSION_LOSS ||
                   abs(gpu_img_in.img_g[x]-img_in.img_g[x])>MAX_CONVERSION_LOSS ||    
                   abs(gpu_img_in.img_b[x]-img_in.img_b[x])>MAX_CONVERSION_LOSS)        
                   {
                        printf("Error yuv2rgb wrong pixel pos:%d abs(r):%d abs(g):%d abs(b):%d\n",x,
                        abs(gpu_img_in.img_r[x]-img_in.img_r[x]),
                        abs(gpu_img_in.img_g[x]-img_in.img_g[x]),
                        abs(gpu_img_in.img_b[x]-img_in.img_b[x]));
                        printf("Error yuv2rgb Original image values:r:%d g:%d b:%d\n",
                                img_in.img_r[x],img_in.img_g[x],img_in.img_b[x]);
                        printf("Error yuv2rgb Converted image values:r:%d g:%d b:%d\n",
                                gpu_img_in.img_r[x],gpu_img_in.img_g[x],gpu_img_in.img_b[x]);
                        return false;
                   }
            }   
            if(!IS_TABULAR)
              printf("Successfully confirmed confirm_gpu_yuv2rgbGPU!\n"); 
            else
              printf("Success\t|");       
            return true;            
        }   

	return false;
}



PPM_IMG read_ppm(const char * path){
    FILE * in_file;
    char sbuf[256];
    
    char *ibuf;
    PPM_IMG result;
    int v_max, i;
    in_file = fopen(path, "r");
    if (in_file == NULL){
        printf("Input file not found!\n");
        exit(1);
    }
    /*Skip the magic number*/
    fscanf(in_file, "%s", sbuf);


    //result = malloc(sizeof(PPM_IMG));
    fscanf(in_file, "%d",&result.w);
    fscanf(in_file, "%d",&result.h);
    fscanf(in_file, "%d\n",&v_max);
    printf("Image size: %d x %d\n", result.w, result.h);
    

    result.img_r = (unsigned char *)malloc(result.w * result.h * sizeof(unsigned char));
    result.img_g = (unsigned char *)malloc(result.w * result.h * sizeof(unsigned char));
    result.img_b = (unsigned char *)malloc(result.w * result.h * sizeof(unsigned char));
    ibuf         = (char *)malloc(3 * result.w * result.h * sizeof(char));

    
    fread(ibuf,sizeof(unsigned char), 3 * result.w*result.h, in_file);

    for(i = 0; i < result.w*result.h; i ++){
        result.img_r[i] = ibuf[3*i + 0];
        result.img_g[i] = ibuf[3*i + 1];
        result.img_b[i] = ibuf[3*i + 2];
    }
    
    fclose(in_file);
    free(ibuf);
    
    return result;
}

void write_yuv(YUV_IMG img, const char * path){//Output in YUV444 Planar
    FILE * out_file;
    int i;
    
    printf("Saving yuv...\n");  
    out_file = fopen(path, "wb");
    fwrite(img.img_y,sizeof(unsigned char), img.w*img.h, out_file);
    fwrite(img.img_u,sizeof(unsigned char), img.w*img.h, out_file);
    fwrite(img.img_v,sizeof(unsigned char), img.w*img.h, out_file);
    fclose(out_file);
    printf("Saved!\n");
}


void write_yuv2(YUV_IMG img, const char * path){ //Output in YUV444 Packed
    FILE * out_file;
    int i;
    
    char * obuf = (char *)malloc(3 * img.w * img.h * sizeof(char));
    
    printf("Saving yuv...\n");

    for(i = 0; i < img.w*img.h; i ++){
        obuf[3*i + 0] = img.img_y[i];
        obuf[3*i + 1] = img.img_u[i];
        obuf[3*i + 2] = img.img_v[i];
    }

    out_file = fopen(path, "wb");
    fwrite(obuf,sizeof(unsigned char), 3*img.w*img.h, out_file);
    fclose(out_file);
    free(obuf);
    printf("Saved!\n");
}


void write_ppm(PPM_IMG img, const char * path){
    FILE * out_file;
    int i;
    
    char * obuf = (char *)malloc(3 * img.w * img.h * sizeof(char));
    printf("Saving rgb...\n");

    for(i = 0; i < img.w*img.h; i ++){
        obuf[3*i + 0] = img.img_r[i];
        obuf[3*i + 1] = img.img_g[i];
        obuf[3*i + 2] = img.img_b[i];
    }
    out_file = fopen(path, "wb");
    fprintf(out_file, "P6\n");
    fprintf(out_file, "%d %d\n255\n",img.w, img.h);
    fwrite(obuf,sizeof(unsigned char), 3*img.w*img.h, out_file);
    fclose(out_file);
    free(obuf);
    printf("Saved!\n");
}

void free_yuv(YUV_IMG img)
{
    free(img.img_y);
    free(img.img_u);
    free(img.img_v);
}

void free_ppm(PPM_IMG img)
{
    free(img.img_r);
    free(img.img_g);
    free(img.img_b);
}



