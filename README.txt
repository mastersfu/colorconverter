Compiling the project:
------------------------------------------
## make clean
## make
------------------------------------------

Executing the project:
(There is one argument to visualize output format)

To view results with more information
------------------------------------------
##./mycode 0 
------------------------------------------

To view with tabluar format with various parameters
------------------------------------------
##./mycode 1 
------------------------------------------

--- Here are the results from the output ----------

Definitions for the table header below: 

Blocks= Number of block in the kernel
Threads= Number of threads per block in the kernel
Spac Alloc= Time to allocate space in GPU cudaMalloc
DataToGPU= Time required to copy data from main memory to GPU
DataToMain= Time required to copy data from GPU to main memory
R-Y Tot Time = RGB to YUV total conversion time 
Y-R Tot time = YUV to RGB total conversion time
R-Y min loss = Success if loss in conversion is less the thresold for RGB to YUV
Y-R min loss = Success if loss in conversion is less the thresold for YUV to RGB

Results:

--------- GPU Processing time based on Blocks and Threads -----------


Blocks	|Threads|Space Alloc	|DataToGPU	|DataToMain	|R-Y Tot Time	|Y-R Tot time	|R-Y min Loss	|Y-R min Loss	|
------------------------------------------------------------------------------------------------------------------------------------
1000	|2	|5.255000	|0.897000	|2.542000	|13.133000	|16.025000	|Success	|Success	|
1000	|4	|3.702000	|0.517000	|0.724000	|7.656000	|15.958000	|Success	|Success	|
1000	|8	|3.446000	|0.489000	|0.709000	|6.635000	|9.620000	|Success	|Success	|
1000	|16	|3.392000	|0.437000	|0.676000	|6.195000	|8.926000	|Success	|Success	|
1000	|32	|3.362000	|0.444000	|0.684000	|5.965000	|8.882000	|Success	|Success	|
1000	|64	|3.462000	|0.424000	|0.647000	|5.989000	|13.793000	|Success	|Success	|
1000	|128	|5.004000	|0.590000	|1.015000	|8.692000	|12.083000	|Success	|Success	|
1000	|256	|3.859000	|0.540000	|0.785000	|6.840000	|9.585000	|Success	|Success	|
1000	|512	|3.435000	|0.446000	|0.693000	|6.082000	|8.914000	|Success	|Success	|
1000	|1024	|3.432000	|0.460000	|0.679000	|6.134000	|8.771000	|Success	|Success	|
2000	|2	|3.384000	|0.460000	|0.703000	|8.382000	|10.596000	|Success	|Success	|
2000	|4	|3.394000	|0.457000	|0.707000	|7.149000	|9.675000	|Success	|Success	|
2000	|8	|3.339000	|0.477000	|0.712000	|6.505000	|9.346000	|Success	|Success	|
2000	|16	|3.403000	|0.477000	|0.686000	|6.219000	|9.066000	|Success	|Success	|
2000	|32	|3.324000	|0.434000	|0.657000	|5.942000	|9.049000	|Success	|Success	|
2000	|64	|3.360000	|0.491000	|0.684000	|6.089000	|8.958000	|Success	|Success	|
2000	|128	|3.351000	|0.452000	|0.699000	|5.991000	|8.917000	|Success	|Success	|
2000	|256	|3.406000	|0.460000	|0.698000	|6.126000	|8.758000	|Success	|Success	|
2000	|512	|3.311000	|0.444000	|0.688000	|5.977000	|8.888000	|Success	|Success	|
2000	|1024	|3.393000	|0.465000	|0.661000	|6.123000	|9.089000	|Success	|Success	|
3000	|2	|3.321000	|0.438000	|0.721000	|8.319000	|10.660000	|Success	|Success	|
3000	|4	|3.306000	|0.509000	|0.725000	|7.131000	|9.539000	|Success	|Success	|
3000	|8	|3.404000	|0.460000	|0.660000	|6.523000	|9.226000	|Success	|Success	|
3000	|16	|3.311000	|0.480000	|0.684000	|6.143000	|9.105000	|Success	|Success	|
3000	|32	|3.430000	|0.419000	|0.671000	|6.062000	|8.947000	|Success	|Success	|
3000	|64	|2.784000	|0.388000	|0.610000	|5.085000	|7.141000	|Success	|Success	|
3000	|128	|2.749000	|0.377000	|0.560000	|4.899000	|7.233000	|Success	|Success	|
3000	|256	|2.768000	|0.376000	|0.531000	|4.920000	|7.049000	|Success	|Success	|
3000	|512	|2.800000	|0.360000	|0.522000	|5.633000	|6.948000	|Success	|Success	|
3000	|1024	|2.831000	|0.348000	|0.545000	|5.067000	|7.135000	|Success	|Success	|
4000	|2	|2.623000	|0.350000	|0.560000	|7.102000	|8.481000	|Success	|Success	|
4000	|4	|2.739000	|0.339000	|0.511000	|6.000000	|7.802000	|Success	|Success	|
4000	|8	|2.720000	|0.371000	|0.537000	|5.312000	|7.215000	|Success	|Success	|
4000	|16	|2.648000	|0.351000	|0.537000	|4.912000	|7.151000	|Success	|Success	|
4000	|32	|2.652000	|0.360000	|0.556000	|4.828000	|7.055000	|Success	|Success	|
4000	|64	|2.798000	|0.343000	|0.522000	|4.995000	|6.984000	|Success	|Success	|
4000	|128	|2.704000	|0.372000	|0.555000	|4.859000	|7.040000	|Success	|Success	|
4000	|256	|2.675000	|0.343000	|0.555000	|4.805000	|7.143000	|Success	|Success	|
4000	|512	|2.610000	|0.374000	|0.581000	|4.858000	|6.899000	|Success	|Success	|
4000	|1024	|2.651000	|0.355000	|0.522000	|4.904000	|7.090000	|Success	|Success	|

